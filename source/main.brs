Function Main()
     port = CreateObject("roMessagePort")
     response = GrabFeed("http://reddit.com/.json")	
     poster = CreateObject("roPosterScreen")
     poster.SetBreadcrumbText("Roku", "Reddit")
     poster.SetMessagePort(port) 
     list = CreateObject("roArray", 10, true) 
     For Each child in response.data.children 
         o = CreateObject("roAssociativeArray")
         o.ContentType = "Reddit Link"
         o.Title = child.data.title
         o.ShortDescriptionLine1 = child.data.title
         o.ShortDescriptionLine2 = "Ups: " + child.data.ups.ToStr() + "   Downs: " + child.data.downs.ToStr()
         o.Description = ""
	 
	 if child.data.url.InStr("imgur")  > 0 
	    if child.data.url.Instr(".gif") > 0
	       print "IS GIF"
	       extension = ".gif"
	    elseif child.data.url.Instr(".jpg") > 0
	       extension = ".jpg"
	    else
	       extension = ".jpg"
	       child.data.url = child.data.url + ".jpg"	
	    end if
	    o.SDPosterUrl = child.data.url.Left(len(child.data.url)-4) + "b" + extension
	    print o.SDPosterUrl
	 else
	    o.SDPosterUrl = child.data.thumbnail
	 end if

         list.Push(o)
     End For
     poster.SetContentList(list)
     poster.Show() 
 
     While True
         msg = wait(0, port)
         If msg.isScreenClosed() Then
             return -1
         ElseIf msg.isListItemSelected()
             print "msg: ";msg.GetMessage();"idx: ";msg.GetIndex()
         End If
     End While
End Function

'**************************************
'@return the RedditFeed for a given url
'**************************************
Function GrabFeed(url as string) as Object
    searchRequest = CreateObject("roUrlTransfer")
    searchRequest.SetUrl(url)
    response = ParseJson(searchRequest.GetToString())
    return response
End Function